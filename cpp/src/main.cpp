#include "classes.h"

#include <fstream>
#include <iostream>

void parseSite(Site& site, const std::string& filename)
{
    std::ifstream f(filename);
    std::string type;
    while (f >> type) {
        if (type == "Customer") {
            std::string userName;
            std::string endOfLine;
            std::string cardNumber;
            f >> userName;
            std::getline(f, endOfLine);
            std::getline(f, cardNumber);
            site.addUser(std::make_shared<Customer>(userName, cardNumber));
        } else if (type == "Employee") {
            std::string userName;
            int startTime;
            int endTime;
            f >> userName >> startTime >> endTime;
            WorkTime workTime = std::make_pair(startTime, endTime);
            site.addUser(std::make_shared<Employee>(userName, workTime));
        } else if (type == "") {
            continue;
        } else {
            std::cerr << "Unknown type: " << type << std::endl;
        }
    }
}

Users doTask(const Site& site)
{
    const Users& users = site.getUsers();
    Users result;

    auto filter = [](std::shared_ptr<User> user) {
        Employee* employee = dynamic_cast<Employee*>(user.get());
        if (employee == nullptr)
            return false;

        return employee->getWorkTime().second > 18;
    };
    std::copy_if(users.begin(), users.end(), std::back_inserter(result), filter);
    return result;
}

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " <file with users>\n";
        return 1;
    }

    Site site("Test site");
    parseSite(site, argv[1]);
    auto users = site.getUsers();

    std::cout << "**Before sort:**" << std::endl;
    for (const auto& user : users)
        std::cout << user->toString() << std::endl;

    std::cout << std::endl << "**After sort:**" << std::endl;
    site.sort();
    for (const auto& user : users)
        std::cout << user->toString() << std::endl;

    std::cout << std::endl << "**Do task:**" << std::endl;
    auto result = doTask(site);
    for (const auto& user : result)
        std::cout << user->toString() << std::endl;

    return 0;
}
