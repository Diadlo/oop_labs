#pragma once

#include <algorithm>
#include <memory>
#include <sstream>
#include <string>
#include <utility> // pair
#include <vector>

using WorkTime = std::pair<int, int>;

class User
{
public:
    User(const std::string& username)
        : m_username{username}
    {}

    const std::string& getUsername() const
    {
        return m_username;
    }

    virtual std::string toString() const = 0;

private:
    std::string m_username;
};


class Customer : public User
{
public:
    Customer(const std::string& username, const std::string& cardNumber)
        : User(username)
        , m_cardNumber(cardNumber)
    {
    }

    const std::string& getCardNumber() const
    {
        return m_cardNumber;
    }

    std::string toString() const override
    {
        std::stringstream ss;
        ss << "Customer " << getUsername() << " (" << m_cardNumber << ")";
        return ss.str();
    }

private:
    std::string m_cardNumber;
};

class Employee : public User
{
public:
    Employee(const std::string& username, const WorkTime& workTime)
        : User(username)
        , m_workTime{workTime}
    {}

    std::string toString() const override
    {
        std::stringstream ss;
        ss << "Employee " << getUsername() << " (" <<
            m_workTime.first << "-" << m_workTime.second << ")";
        return ss.str();
    }

    WorkTime getWorkTime() const
    {
        return m_workTime;
    }
private:
    WorkTime m_workTime;
};

using Users = std::vector<std::shared_ptr<User>>;

class Site
{
public:
    Site(const std::string& sitename)
        : m_sitename{sitename}
    {
    }

    void addUser(std::shared_ptr<User> user)
    {
        m_users.push_back(user);
    }

    Users getUsers() const
    {
        return m_users;
    }

    void sort()
    {
        auto comparator = [](const auto& a, const auto& b) {
            return a->getUsername() < b->getUsername();
        };
        std::sort(m_users.begin(), m_users.end(), comparator);
    }

private:
    std::string m_sitename;
    Users m_users;
};
