/**
 * Created by Танечка on 15.09.2019.
 */
public class Employee extends User {

    private int workFrom;
    private int workUntil;
    public Employee(String userName, int workFrom, int workUntil) {
        super(userName);
        this.workFrom = workFrom;
        this.workUntil = workUntil;
    }

    @Override
    public String toString() {
        String out = "Employee " + getUserName() + " (" + workFrom + "-" + workUntil +")";
        return out;
    }

    public int getWorkFrom() {
        return workFrom;
    }

    public void setWorkFrom(int workFrom) {
        this.workFrom = workFrom;
    }

    public int getWorkUntil() {
        return workUntil;
    }

    public void setWorkUntil(int workUntil) {
        this.workUntil = workUntil;
    }
}
