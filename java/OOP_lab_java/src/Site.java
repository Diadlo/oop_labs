import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Танечка on 15.09.2019.
 */
public class Site {
    private String siteName;
    private List<User> users;

    public Site(String siteName){
        this.siteName = siteName;
        users = new ArrayList<>();
    }

    public void addUser(User user){
        users.add(user);
    }

    public List<User> getUsers(){
        return users;
    }

    public void sort(){
        users.sort(new Comparator<User>() {
            public int compare(User u1, User u2) {
                //return u1.toString().compareTo(u2.toString());
                return u1.getUserName().compareTo(u2.getUserName());
            }
        });
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }
}
