/**
 * Created by Танечка on 15.09.2019.
 */
public class Customer extends User {

    private String cardNumber;

    public Customer(String userName, String cardNumber) {
        super(userName);
        this.cardNumber = cardNumber;
    }

    @Override
    public String toString() {
        String out = "Customer " + getUserName() + " (" + cardNumber + ")";
        return out;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
