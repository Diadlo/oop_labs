import java.io.*;
import java.util.Iterator;
import java.util.List;

public class Main {

    public static void parseSite(Site site, String fileName) {
        try {
            FileInputStream fileInputStream = null;
            fileInputStream = new FileInputStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream, "Cp1251"));
            String userType;
            while ((userType = br.readLine()) != null) {
                if(userType.equals("Customer")){
                    String userName, cardNumber;
                    userName = br.readLine();
                    cardNumber = br.readLine();
                    site.addUser(new Customer(userName, cardNumber));
                }
                else if(userType.equals("Employee")){
                    String userName, workFrom, workUntil;
                    userName = br.readLine();
                    workFrom = br.readLine().trim();
                    workUntil = br.readLine().trim();
                    site.addUser(new Employee(userName, Integer.valueOf(workFrom), Integer.valueOf(workUntil)));
                }
                else if (!userType.equals("")) {
                    System.out.println("Unknown user type");
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Файл " + fileName + " не найден.");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IOException");
            e.printStackTrace();
        }
    }
    public static void doTask(Site site){
        List<User> users = site.getUsers();
        Iterator<User> userIterator = users.iterator();
        while(userIterator.hasNext()) {
            User user = userIterator.next();
            if(user instanceof Employee){
                if(((Employee) user).getWorkUntil() >= 18)
                    userIterator.remove();
            }
        }

    }
    public static void main(String[] args) {
        Site site = new Site("Test site");
        String fileName = "data.txt";
        parseSite(site, fileName);
        List<User> users = site.getUsers();

        System.out.println("**Before sort:**");
        for(User user: users){
            System.out.println(user.toString());
        }

        System.out.println("**After sort:**");
        site.sort();
        for(User user: users){
            System.out.println(user.toString());
        }

        System.out.println("**Do task:**");
        doTask(site);
        for(User user: users){
            System.out.println(user.toString());
        }
    }
}
