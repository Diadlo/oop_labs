﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab
{
    public class Employee : User
    {
        public int WorkFrom { get; private set; }
        public int WorkUntil { get; private set; }
        public Employee(string username, int workFrom, int workUntil)
            : base(username)
        {
            WorkFrom = workFrom;
            WorkUntil = workUntil;
        }

        public override string ToString()
        {
            return $"Employee {Username} ({WorkFrom} - {WorkUntil})";
        }
    }
}
