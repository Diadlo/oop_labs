﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab
{
    public abstract class User
    {
        public string Username { get; private set; }
        public User(string username)
        {
            Username = username;
        }

        public new abstract string ToString();
    }
}
