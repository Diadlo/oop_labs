﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab
{
    using Users = List<User>;
    public class Site
    {
        public string SiteName { get;  set; }
        public Users Users { get; private set; }

        public Site(string siteName)
        {
            Users = new Users();
            SiteName = siteName;
        }

        public void AddUser(User user)
        {
            Users.Add(user);
        }

        public void Sort()
        {
            Users.Sort((User left, User right) => left.Username.CompareTo(right.Username));
        }
    }
}
