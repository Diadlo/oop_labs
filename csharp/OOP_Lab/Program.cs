﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab
{
    using Customers = IEnumerable<Customer>;
    class Program
    {
        static void ParseSite(ref Site site, string fileName)
        {
            var lines = File.ReadAllLines(fileName);
            for (int i = 0; i < lines.Length;)
            {
                if (lines[i] == "Customer")
                {
                    site.AddUser(new Customer(lines[i + 1], lines[i + 2]));
                    i += 4;
                }
                else if (lines[i] == "Employee")
                {
                    site.AddUser(new Employee(lines[i + 1], int.Parse(lines[i + 2]), int.Parse(lines[i + 3])));
                    i += 5;
                }
                else
                {
                    i++;
                }
            }
        }

        static Customers GetCustomers(Site site)
        {
            return site.Users.Select((user) => user as Customer).OfType<Customer>();
        }

        static void DoTask(Site site)
        {
            site.Users.RemoveAll((user) =>
            {
                var employee = user as Employee;
                if (employee == null)
                    return false;
                return employee.WorkUntil > 18;
            });
        }

        static void Main(string[] args)
        {
            Site site = new Site("Test site");
            ParseSite(ref site, "data.txt");

            Console.WriteLine("Before sort:");
            foreach (var user in site.Users)
            {
                Console.WriteLine(user.ToString());
            }

            site.Sort();

            Console.WriteLine("\nAfter sort:");
            foreach (var user in site.Users)
            {
                Console.WriteLine(user.ToString());
            }

            Console.WriteLine("\nDo task:");
            DoTask(site);
            foreach (var customer in site.Users)
            {
                Console.WriteLine(customer.ToString());
            }
            Console.ReadLine();
        }
    }
}
