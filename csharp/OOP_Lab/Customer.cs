﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Lab
{
    public class Customer : User
    {
        public string CardNumber { get; private set; }
        public Customer(string username, string cardNumber)
            : base(username)
        {
            CardNumber = cardNumber;
        }

        public override string ToString()
        {
            return $"Customer {Username} ({CardNumber})";
        }
    }
}
