from abc import ABC, abstractmethod

class User(ABC):
    def __init__(self, username):
        self.__username = username

    def get_username(self):
        return self.__username

    @abstractmethod
    def __str__(self):
        pass

class Customer(User):
    def __init__(self, username, card_number):
        User.__init__(self, username)
        self.__card_number = card_number

    def get_card_number(self):
        return self.__card_number

    def __str__(self):
        return "User: {}".format(self.get_username())

class Employee(User):
    def __init__(self, username, work_time):
        User.__init__(self, username)
        self.__work_time = work_time

    def get_worktime(self):
        return self.__work_time

    def __str__(self):
        return "Employee: {} ({} - {})".format(
                self.get_username(),
                self.__work_time[0],
                self.__work_time[1])

class Site:
    def __init__(self, sitename):
        self.__sitename = sitename
        self.__users = []

    def get_sitename(self):
        return self.__sitename

    def get_users(self):
        return self.__users

    def add_user(self, user):
        self.__users.append(user)

    def sort(self, key):
        self.__users = sorted(self.__users, key=key)
