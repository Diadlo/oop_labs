from classes import *
import sys

def parse_site(site, filename):
    with open(filename) as f:
        while True:
            user_type = f.readline()
            if user_type == '':
                break

            user_type = user_type.strip()
            if user_type == '':
                continue
            elif user_type == "Customer":
                username = f.readline().strip()
                card_number = f.readline().strip()
                site.add_user(Customer(username, card_number))
            elif user_type == "Employee":
                username = f.readline().strip()
                start_time = int(f.readline())
                end_time = int(f.readline())
                site.add_user(Employee(username, (start_time, end_time)))
            else:
                print("Unknown type: '{}'".format(user_type))

def do_task(users):
    filter_func = lambda x: (
        isinstance(x, Employee) and x.get_worktime()[1] > 18
    )
    return filter(filter_func , users)

def main(filename):
    site = Site('Test site')
    parse_site(site, filename)
    print("**Before sort:**")
    for user in site.get_users():
        print(str(user))

    print()
    print("**After sort:**")
    site.sort(key=User.get_username)
    for user in site.get_users():
        print(str(user))

    print()
    print("**After task:**")
    filtered = do_task(site.get_users())
    for user in filtered:
        print(str(user))

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('Usage: {} <file with users>'.format(sys.argv[0]))
        sys.exit(1)

    main(sys.argv[1])
